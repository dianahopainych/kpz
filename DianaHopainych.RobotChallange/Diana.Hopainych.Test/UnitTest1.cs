﻿using System;
using System.Collections.Generic;
using DianaHopainych.RobotChallange;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Diana.Hopainych.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodCollectEnergy()
        {
      
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 100 });


            RobotCommand c = DoStep(m, robots, 0);
            Assert.IsTrue(c is CollectEnergyCommand);
        }

//----------------------------------------------------------------------------------------------------------
      
        [TestMethod]
        public void TestMethodWithNullEnergy()
        {

            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 0 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 0 });


            RobotCommand c = DoStep(m, robots, 0);
            Assert.IsTrue(c is CollectEnergyCommand);
        }


//------------------------------------------------------------------------------------------------------------
     
        [TestMethod]
        public void TestMethodWithNullPosition()
        {

            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(0, 0), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 100 });


            RobotCommand c = DoStep(m, robots, 0);
            Assert.IsTrue(c is CollectEnergyCommand);
        }

//-------------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void TestMethodCreateNewRobot()
        {

            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 1000 });


            RobotCommand c = DoStep(m, robots, 0);
            Assert.IsTrue(c is CreateNewRobotCommand);
        }

        //-----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void TestMethodEqualRobots()
        {

            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(0, 0), Energy = 1000 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 1000 });


            RobotCommand c = DoStep(m, robots, 0);
            Assert.IsTrue(c is CreateNewRobotCommand);
        }

        //-----------------------------------------------------------------------------------------------------------



        [TestMethod]
        public void TestMethodMove()
        {

            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(50, 50), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 1000 });


            RobotCommand c = DoStep(m, robots, 0);
            Assert.IsTrue(c is CreateNewRobotCommand);
        }

//-----------------------------------------------------------------------------------------------
     
        [TestMethod]
        public void TestDistanceHelper()
        {

            var p1 = new Position(1, 1);
            var p2 = new Position(2, 4);
            Assert.AreEqual(10, DistanceHelper.FindDistance(p1, p2));
        }

//-----------------------------------------------------------------------------------------------

        [TestMethod]
        public void TestDistanceHelperNull()
        {

            var p1 = new Position(0, 0);
            var p2 = new Position(0, 0);
            Assert.AreEqual(0, DistanceHelper.FindDistance(p1, p2));
        }

        //-----------------------------------------------------------------------------------------------

        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new DianaHopainychAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                         {new Robot.Common.Robot() {Energy = 200,
                          Position = new Position(2, 3)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
            
        }

        //------------------------------------------------------------------------------------------------
        [TestMethod]
        public void TestMoveEmptyStation()
        {
            var algorithm = new DianaHopainychAlgorithm();
            var map = new Map();
            var stationPosition = new Position(0, 0);
            map.Stations.Add(new EnergyStation() { Energy = 0, Position = stationPosition, RecoveryRate = 0 });
            var robots = new List<Robot.Common.Robot>()
                         {new Robot.Common.Robot() {Energy = 100,
                          Position = new Position(2, 3)}};
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);

        }

//--------------------------------------------------------------------------------------------------------------------

        private RobotCommand DoStep(Map map, List<Robot.Common.Robot> robots, int v)
        {

            if(robots[v].Energy > 300)
                return new CreateNewRobotCommand();
            var stations = map.GetNearbyResources(robots[v].Position, 50);

          
            if (stations.Count >= 1)
                return new CollectEnergyCommand(); 


            throw new NotImplementedException();


        }


    }
}
