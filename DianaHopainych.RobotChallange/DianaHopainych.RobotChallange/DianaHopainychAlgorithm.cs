﻿using Robot.Common;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DianaHopainych.RobotChallange
{
    public class DianaHopainychAlgorithm : IRobotAlgorithm
    {
        public int myRobotsToMoveIndex { get; set; }

        public string Author
        {
            get { return "Diana Hopainych"; }
        }

        public string Description
        {
            get
            {
                return String.Concat("This is a simple algorithm.", "Move robot");
            }

        }

        //-------------------------------------------------------------------------------------------
        public bool MyStation(Robot.Common.Robot myRobot, Map map, IList<Robot.Common.Robot> myRobots)
        {
            using (IEnumerator<EnergyStation> enumerator = ((IEnumerable<EnergyStation>)map.Stations).GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    EnergyStation current = enumerator.Current;
                    if (myRobot.Position == current.Position)
                        return true;
                }
            }
            return false;
        }
      
//---------------------------------------------------------------------------------------------------------------------------
       public Position PositionOfStation(Robot.Common.Robot myRobot, Map map, IList<Robot.Common.Robot> myRobots)
        {
            int num = 2000000000;
            Position myPosition = null;
            using (IEnumerator<EnergyStation> enumerator = ((IEnumerable<EnergyStation>)map.Stations).GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    EnergyStation current = enumerator.Current;
                  if (this.IsStationFree(myRobot, current, myRobots))
                  {
                        int myDistance = DistanceHelper.FindDistance(current.Position, myRobot.Position);
                        if (myDistance < num)
                        {
                            num = myDistance;
                            myPosition = current.Position;
                       }
                  }
                }
            }
            return myPosition;
        }
     
//---------------------------------------------------------------------------------------------------------------------------
        public bool IsStationFree(Robot.Common.Robot myRobot, EnergyStation myEnergyStation, IList<Robot.Common.Robot> myRobots)
        {
            for (int X = myEnergyStation.Position.X - 2; X <= myEnergyStation.Position.X + 2; ++X)
            {
                for (int Y = myEnergyStation.Position.Y - 2; Y <= myEnergyStation.Position.Y + 2; ++Y)
                {
                       foreach(var robot in myRobots)
                            if (robot != myRobot)
                            if (robot.Position == new Position(X, Y ))
                                return false;
                }
            }
            return true;
        }

//---------------------------------------------------------------------------------------------------------------------------------
        public bool IsCellFree(Position cell, Robot.Common.Robot myRobot,
               IList<Robot.Common.Robot> myRobots)
        {
            foreach (var robot in myRobots)
            {
                if (robot != myRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        //---------------------------------------------------------------------------------------------------------------------
        public Position CalculateMyNewPosition(Position presentPosition, Position nextPosition, int MaxDistanceEnergy)
        {
           Position myNewPosition = new Position(nextPosition.X, nextPosition.Y);
          
                if (DistanceHelper.FindDistance(presentPosition, myNewPosition) < MaxDistanceEnergy)
                   return myNewPosition;
                myNewPosition.X = (presentPosition.X + (int)((double)(myNewPosition.X - presentPosition.X) / 2.0));
                myNewPosition.Y = (presentPosition.Y + (int)((double)(myNewPosition.Y - presentPosition.Y) / 2.0));
          
            return myNewPosition.X < 0 || myNewPosition.X > 50 || myNewPosition.Y < 0 || myNewPosition.Y > 50 ? presentPosition : myNewPosition;
        }
      
        //---------------------------------------------------------------------------------------------------------------------------------
       public Position MyNewPlace(IList<Robot.Common.Robot> myRobots, Robot.Common.Robot myRobot, Position myNewPosition)
        {
            int MaxDistanceEnergy = myRobot.Energy > 10 ? 300 : myRobot.Energy;
            Position nextPosition = new Position(myNewPosition.X, myNewPosition.Y);
           Position myPosition = new Position(myRobot.Position.X, myRobot.Position.Y);
            if (Math.Abs(myPosition.X - nextPosition.X) + Math.Abs(myPosition.Y - nextPosition.Y) + 50 > MaxDistanceEnergy)
             return null;
             return  CalculateMyNewPosition(myRobot.Position, nextPosition, MaxDistanceEnergy);
        }
    

//--------------------------------------------------------------------------------------------------------------------------
        public RobotCommand DoStep(IList<Robot.Common.Robot> myRobots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot myRobot = myRobots[robotToMoveIndex];
            if (MyStation(myRobot, map, myRobots))
            {
                if (myRobot.Energy <= 300 || ((ICollection<Robot.Common.Robot>)myRobots).Count >= ((ICollection<EnergyStation>)map.Stations).Count || this.myRobotsToMoveIndex >= 40)
                    return new CollectEnergyCommand();
                CreateNewRobotCommand createNewRobotCommand = new CreateNewRobotCommand();
                createNewRobotCommand.NewRobotEnergy = 100;
                return createNewRobotCommand;
            }
            Position myNewPosition = this.PositionOfStation(myRobot, map, myRobots);
            if (myNewPosition == null)
            {
                Position myPosition = new Position();
                myPosition.X = (myRobot.Position.X + 1);
               myPosition.Y = (myRobot.Position.Y + 1);
               myNewPosition = myPosition;
            }
            Position presentPosition = null;
            if (myNewPosition != null)
                presentPosition = this.MyNewPlace(myRobots, myRobot, myNewPosition);
            if (presentPosition ==  null)
               return new CollectEnergyCommand();
            MoveCommand moveCommand = new MoveCommand();
            moveCommand.NewPosition = presentPosition;
            return moveCommand;
        }
    }

//***************************************************************************************************************
public class DistanceHelper
    {
        public static int FindDistance(Position currentPosition, Position myRobotPosition)
        {
            return (int)(Math.Pow(currentPosition.X - myRobotPosition.X, 2) + Math.Pow(currentPosition.Y - myRobotPosition.Y, 2));
        }

    }


}
