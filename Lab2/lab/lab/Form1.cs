﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab
{
    public partial class Form1 : Form
    {
        public static double res;
        delegate void UI();

        class MyEvent
        {
            // подія
            public event UI UserEventPlus;
            public event UI UserEventSub;
            public event UI UserEventMul;
            public event UI UserEventDiv;
            public event UI UserEventPercent;
            public event UI UserEventPow;


            //метод для запуску події
            public void OnUserEventPlus()
            {
                UserEventPlus();
            }
            public void OnUserEventSub()
            {
                UserEventSub();
            }
            public void OnUserEventMul()
            {
                UserEventMul();
            }
            public void OnUserEventDiv()
            {
                UserEventDiv();
            }
            public void OnUserEventPercent()
            {
                UserEventPercent();
            }
            public void OnUserEventPow()
            {
                UserEventPow();
            }


        }


//-------------------------------------------------------
        class Plus: Form1
        {

            double a;
            double b;
           

            public Plus(double a, double b)
            {
                this.a = a;
                this.b = b;
              
            }

            public double ExecutionPlus
            {
                set
                {
                    a = value;
                    b = value;
                  
                }
                get
                {
                    double s;
                    s = a + b;
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerPlus()
            {
              
                res = ExecutionPlus;
            }
        }


        //---------------------------------------------------------



        class Sub : Form1
        {

            double a;
            double b;


            public Sub(double a, double b)
            {
                this.a = a;
                this.b = b;

            }

            public double ExecutionSub
            {
                set
                {
                    a = value;
                    b = value;

                }
                get
                {
                    double s;
                    s = a - b;
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerSub()
            {

                res = ExecutionSub;
            }
        }


        //---------------------------------------------------------


        class Mul : Form1
        {

            double a;
            double b;


            public Mul(double a, double b)
            {
                this.a = a;
                this.b = b;

            }

            public double ExecutionMul
            {
                set
                {
                    a = value;
                    b = value;

                }
                get
                {
                    double s;
                    s = a * b;
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerMul()
            {

                res = ExecutionMul;
            }
        }


        //---------------------------------------------------------


        class Div : Form1
        {

            double a;
            double b;


            public Div(double a, double b)
            {
                this.a = a;
                this.b = b;

            }

            public double ExecutionDiv
            {
                set
                {
                    a = value;
                    b = value;

                }
                get
                {
                    double s;
                    if (b != 0)
                        s = a / b;
                    else s = 0;
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerDiv()
            {

                res = ExecutionDiv;
            }
        }


        //---------------------------------------------------------
        class Percent : Form1
        {

            double a;
            double b;


            public Percent(double a, double b)
            {
                this.a = a;
                this.b = b;

            }

            public double ExecutionPercent
            {
                set
                {
                    a = value;
                    b = value;

                }
                get
                {
                    double s;
                    s = a /100 * b;
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerPercent()
            {

                res = ExecutionPercent;
            }
        }


        //---------------------------------------------------------


        class Pow : Form1
        {

            double a;
            double b;


            public Pow(double a, double b)
            {
                this.a = a;
                this.b = b;

            }

            public double ExecutionPow
            {
                set
                {
                    a = value;
                    b = value;

                }
                get
                {
                    double s;
                    s = Math.Pow(a, b);
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerPow()
            {

                res = ExecutionPow;
            }
        }


        //---------------------------------------------------------

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a =0, b = 0;
            label1.Text = "";
            MyEvent evt = new MyEvent();
            if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
            {
                a = Convert.ToDouble(textBox1.Text);
                if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
                {
                    b = Convert.ToDouble(textBox2.Text);
                }
                else
                {
                    label1.Text = "Enter only numbers!";
                    b = 0;
                }

            }
            else
            {
                label1.Text = "Enter only numbers!";
                a = 0;
            }

            Plus userPlus = new Plus(a, b);
            //обробник події
            evt.UserEventPlus += userPlus.UserInfoHandlerPlus;
            // зарущена подія
            evt.OnUserEventPlus();
            label1.Text = Convert.ToString(res);
         


        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            double a = 0, b = 0;
            label1.Text = "";
            MyEvent evt = new MyEvent();
            if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
            {
                a = Convert.ToDouble(textBox1.Text);
                if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
                {
                    b = Convert.ToDouble(textBox2.Text);
                }
                else
                {
                    label1.Text = "Enter only numbers!";
                    b = 0;
                }

            }
            else
            {
                label1.Text = "Enter only numbers!";
                a = 0;
            }

            Sub userSub = new Sub(a, b);
            //обробник події
            evt.UserEventSub += userSub.UserInfoHandlerSub;
            // зарущена подія
            evt.OnUserEventSub();
            label1.Text = Convert.ToString(res);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double a = 0, b = 0;
            label1.Text = "";
            MyEvent evt = new MyEvent();
            if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
            {
                a = Convert.ToDouble(textBox1.Text);
                if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
                {
                    b = Convert.ToDouble(textBox2.Text);
                }
                else
                {
                    label1.Text = "Enter only numbers!";
                    b = 0;
                }

            }
            else
            {
                label1.Text = "Enter only numbers!";
                a = 0;
            }

            Mul userMul = new Mul(a, b);
            //обробник події
            evt.UserEventMul += userMul.UserInfoHandlerMul;
            // зарущена подія
            evt.OnUserEventMul();
            label1.Text = Convert.ToString(res);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double a = 0, b = 0;
            label1.Text = "";
            MyEvent evt = new MyEvent();
            if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
            {
                a = Convert.ToDouble(textBox1.Text);
                if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
                {
                    b = Convert.ToDouble(textBox2.Text);
                }
                else
                {
                    label1.Text = "Enter only numbers!";
                    b = 0;
                }

            }
            else
            {
                label1.Text = "Enter only numbers!";
                a = 0;
            }

            Div userDiv = new Div(a, b);
            //обробник події
            evt.UserEventDiv += userDiv.UserInfoHandlerDiv;
            // зарущена подія
            evt.OnUserEventDiv();
            label1.Text = Convert.ToString(res);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            double a = 0, b = 0;
            label1.Text = "";
            MyEvent evt = new MyEvent();
            if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
            {
                a = Convert.ToDouble(textBox1.Text);
                if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
                {
                    b = Convert.ToDouble(textBox2.Text);
                }
                else
                {
                    label1.Text = "Enter only numbers!";
                    b = 0;
                }

            }
            else
            {
                label1.Text = "Enter only numbers!";
                a = 0;
            }

            Percent userPercent = new Percent(a, b);
            //обробник події
            evt.UserEventPercent += userPercent.UserInfoHandlerPercent;
            // зарущена подія
            evt.OnUserEventPercent();
            label1.Text = Convert.ToString(res);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            double a = 0, b = 0;
            label1.Text = "";
            MyEvent evt = new MyEvent();
            if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
            {
                a = Convert.ToDouble(textBox1.Text);
                if (Regex.IsMatch(textBox1.Text, @"^\d+$"))
                {
                    b = Convert.ToDouble(textBox2.Text);
                }
                else
                {
                    label1.Text = "Enter only numbers!";
                    b = 0;
                }

            }
            else
            {
                label1.Text = "Enter only numbers!";
                a = 0;
            }

            Pow userPow = new Pow(a, b);
            //обробник події
            evt.UserEventPow += userPow.UserInfoHandlerPow;
            // зарущена подія
            evt.OnUserEventPow();
            label1.Text = Convert.ToString(res);




        }
    }
}
