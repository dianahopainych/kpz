﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab
{
    public partial class Form2 : Form
    {

        delegate void UI();
        public static String res;

        class MyEvent
        {
            // подія
            public event UI UserEventPlus;
            public event UI UserEventNewForm;



            //метод для запуску події
            public void OnUserEventPlus()
            {
                UserEventPlus();
            }
            public void OnUserEventNewForm()
            {
                UserEventNewForm();
            }

        }

        //-------------------------------------------------

        class Pluss : Form2
        {

            String a;
            String b;


            public Pluss( String a, String b)
            {
                this.a = a;
                this.b = b;

            }

            public String Execution
            {
                set
                {
                    a = value;
                    b = value;

                }
                get
                {
                    String s;
                    s = a + b;
                    return s;
                }
            }

            // обробник події
            public void UserInfoHandlerPluss()
            {

                res = Execution;
            }
        }


        //---------------------------------------------------------


        class NewForm : Form2
        {

            public NewForm()
            {

            }

            public void ExecutionNewForm()
            {
                Form1 form = new Form1();
                form.Show();
            }

            // обробник події
            public void UserInfoHandlerNewForm()
            {

                ExecutionNewForm();
            }
        }


        //---------------------------------------------------------


        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            MyEvent evt = new MyEvent();

            Pluss userPluss = new Pluss("Hello, ", "Friend!");
            //обробник події
            evt.UserEventPlus += userPluss.UserInfoHandlerPluss;
            // зарущена подія
            evt.OnUserEventPlus();
            label1.Text = res;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyEvent evt = new MyEvent();

            NewForm userNewForm = new NewForm();
            //обробник події
            evt.UserEventNewForm += userNewForm.UserInfoHandlerNewForm;
            // зарущена подія
            evt.OnUserEventNewForm();
            //label1.Text = res;

        }
    }
}
