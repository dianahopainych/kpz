﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Globalization;

namespace lab3
{
    //клас сервіс
    class Service
    {
        public int id;
        public string name;
        public float price;

        public Service(int myId, string myName, float myPrice)
        {
            id = myId;
            name = myName;
            price = myPrice;

        }


    }

    //анонімний клас салон

    class Salon
    {
   
        public string Name { get; set; }
        public int Id { get; set; }
        public string Adress { get; set; }

    }

    //-------------------------------------------------------------


    class Customers
    {

        public int ID { get; set; }
        public string Name { set; get; }
      

        public Customers() { }
        public Customers( int ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
          
        }

        public override string ToString()
        {
            return String.Format("ID {0}\t Name : {1}", this.ID, this.Name);
        }
    }

    class CompInv<T> : IComparer<T>
        where T : Customers
    {
        //  IComparer<T>
        public int Compare(T x, T y)
        {
            if (x.ID < y.ID)
                return 1;
            if (x.ID > y.ID)
                return -1;
            else return 0;
        }
    }


    //--------------------------------------------------------------------

    class Program
    {
        static void Main(string[] args)
        {


            string[] names = { "Manicure", "Hairdress", "Make up", "Women haircut", "Men haircut", "Kids haircut" };
            float[] prices = { 350.0f, 600.0f, 550.0f, 300.0f, 200.0f, 250.0f };

            //масив з об'єктів класу 
            Service[] MyServices = new Service[6];
            for (int i = 0; i < 6; i++)
                MyServices[i] = new Service(i + 1, names[i], prices[i]);

            //ліст з об'єктів класу 
            List<Service> MyList = new List<Service>();
            for (int i = 0; i < 6; i++)
                MyList.Add(new Service(i + 1, names[i], prices[i]));

            //словник з об'єктів класу 
            Dictionary<int, Service> MyDictionary = new Dictionary<int, Service>();
            for (int i = 0; i < 6; i++)
                MyDictionary.Add(i + 1, new Service(i + 1, names[i], prices[i]));


            //селект з масиву  об'єктів класу, де ід  більше, ніж 3, сортовано по довжині імені та записано у зворотньому порядку
            var selectH = MyServices.Where(t => t.id > 3).OrderBy(t => t.name.Length).Reverse();
            Console.WriteLine("\nFrom array:");
            foreach (var i in selectH)
            {
                Console.WriteLine("{0} - {1}" , i.id, i.name);
            }

            //селект з ліста  об'єктів класу, де назва починається з "М" 
            IEnumerable<string> selectM = from n in MyList
                                          where n.name.StartsWith("M")
                                          select n.name;
            Console.WriteLine("\nFrom List:");
            foreach (string name in selectM)
            {
                Console.WriteLine("{0}", name);
            }

            //селект зі словника об'єктів класу, де назва починається з "W" 
            IEnumerable<string> selectW = from n in MyDictionary
                                          where n.Value.name.StartsWith("W")
                                          select n.Value.name;

  

            Console.WriteLine("\nFrom Dictionary:");
            foreach (string name in selectW)
            {
                Console.WriteLine("{0}", name);
            }

            //селект у словник ід та назви сервісу  
            Dictionary<int, string> eDictionary =
            MyServices.ToDictionary(k => k.id,
                                         i => string.Format("{0} - {1}",
                                         i.id, i.name));
            Console.WriteLine("\nTo Dictionary:");
            string named = eDictionary[2];
            Console.WriteLine(named);

            //-------------------------------------------------------------------------------------

            //anonymous 
            Salon[] MySalons = new Salon[3];
            MySalons[0] = new Salon { Id = 1, Name = "Paris", Adress = "Shevchenko Street, 28" };
            MySalons[1] = new Salon { Id = 2, Name = "Letual'", Adress = "Mazepa Street, 11" };
            MySalons[2] = new Salon { Id = 3, Name = "New York", Adress = "Bandera Street, 120" };
            Console.WriteLine("\nFrom Salons:");

            var selectSalons = from n in MySalons
                          where n.Name.StartsWith("P")
                          select n.Name;
            foreach (var i in selectSalons)
            {
                Console.WriteLine(i);
            }

            //------------------------------------------------------------------------------------------------
            // initialization

            var Customers = new[]
            {
                new {Id = 1, Name="John"},
                new {Id = 2, Name="Michael"},
                new {Id = 5, Name = "Linda"},
                new {Id = 3, Name = "Anna"},
                new {Id = 4, Name = "Leila"},
                new {Id = 6, Name = "Jack"}

            };

            var selectCus =
    from cus in Customers where cus.Name.StartsWith("L")
    select new { cus.Name };


           
            Console.WriteLine("\nCustomer's names what start with 'L': ");

            foreach (var v in selectCus)
            {
                Console.WriteLine("{0}", v.Name);
            }



            var selectCustomers = Customers.Where(t => t.Id > 3).OrderBy(t => t.Id);

            Console.WriteLine("\nCustomers with Id bigger than 3, ordered by Id: ");

            foreach (var v in selectCustomers)
            {
                Console.WriteLine("{0} - {1}", v.Id, v.Name);
            }

            //-----------------------------------------------------------------------------
            
            // Comparer

            CompInv<Customers> cp = new CompInv<Customers>();
            List<Customers> dic = new List<Customers>();

            
            dic.Add(new Customers(3, "John"));
            dic.Add(new Customers(2, "Michael"));
            dic.Add(new Customers(4, "Linda"));
            dic.Add(new Customers(5, "Leila"));
            dic.Add(new Customers(1, "Jack"));

           // Console.WriteLine("\nFirst List:");
           // foreach (Customers a in dic)
           //     Console.WriteLine(a);

            Console.WriteLine("\nSorted Customers:");
            dic.Sort(cp);
            foreach (Customers a in dic)
                Console.WriteLine(a);
            //----------------------------------------------------------------------------

            // List to array
            Service[] MyServices1 = MyList.ToArray();

            Console.WriteLine("\nList to array:");
            for (int i = 0; i < 6; i++)
                Console.WriteLine("{0} - {1}, price = {2}", MyServices1[i].id, MyServices1[i].name, MyServices1[i].price);



        }
    }
}

