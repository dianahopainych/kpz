﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Dynamic;

namespace ConsoleApplication2
{
    //клас для реалізації динамічного конструктора
    public class DynamicClass : DynamicObject { }


    class Person
    {
        public int id;
        public string firstName;
        private static string phoneNumber;
       //статичний конструктор(тільки для статичних змінних)
        static Person()
        {
            phoneNumber = "";
        }
        // перевантажені конструктори
        public Person() { }
        public Person(string firstName)
        {
            this.firstName = firstName;
            Console.WriteLine("My name is " + firstName);
        }
        public Person(int id)
        {
            this.id = id;
           
        }
        public Person(string firstName,  int id)
        {
            this.firstName = firstName;
            this.id = id;
        }
        //методи для демонстрації out i ref
        //реалізовує доступ до елемента за посиланням
        public void SomeMessageRef(ref string mes)
        {
            Console.WriteLine(mes);
        }
        //реалізовує доступ до елемента за посиланням, проте обов"язково потрібна ініціалізація 
        //всередині методу
        public void SomeMessageOut(out string mes)
        {
            mes = "It is parameter out ";
           // Console.WriteLine(mes);
        }

    }
  
      
    class Customer : Person
    {
        //демонстрація використання base
        public Customer(string firstName, string lastName)
            : base(firstName)
        {
            Console.WriteLine("My surname is " + lastName);
        }

        //демонстрація використання this
        public Customer() : this("Lilly", "Evans")
        {
           
        }
    }

    //-----------------------------------------------------
    //перелічуваний тип для булівських операторів
    enum Professions
    {
        HairDresser = 1,
        Colorist,
        MasterOfManicure,
        MasterOfPedicure
    }

//------------------------------------
    class BeautySalon
    {
        //демонстрація implicit i explicit
        //явне та неявне приведення до типу
        public string Name{ get; set; }
        //явне
        public static implicit operator BeautySalon(string x)
        {
            return new BeautySalon { Name = x };
        }
        //неявне
        public static explicit operator string(BeautySalon salon)
        {
            return salon.Name;
        }
    }
    //--------------------------------------------------------------------

   
    //-----------------------------------------------------------------
    
    interface IEmployee
    {

        public double DoWork()
        {
            return 0;
        }
        public double GetSalary() { return 0; }

       




    }
    //----------------------------------------------------------------

    public class HairDresser : IEmployee
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Profession { get; set; }
        public double Sallary { get; set; }


        public  double DoWork()
        {

            Console.WriteLine("Haircut is done!");
            return 100.0f;

        }


        public double GetSalary() { return this.Sallary; }

    }

    public class Colorist : HairDresser
    {


        public double DoWorkColoring()
        {
            Console.WriteLine("Coloring is done!");
                return 200.0f;
        }

        public double GetSalary() { return this.Sallary; }


    }

    public class Master : IEmployee
    {
        private int id { get; set; }
        private string FirstName { get; set; }
        string LastName { get; set; }
        public string Profession { get; set; }
        protected double Sallary { get; set; }
       
    }


    public abstract class NailMaster: Master
    {

      
        public abstract double DoWork();

        public abstract double GetSalary();
    }

    public class MasterOfManicure: NailMaster
    {

       
        public override double DoWork()
        {
            Console.WriteLine("Manicure is done!");
            return 250.0f;
        }

        public override double GetSalary() { return this.Sallary; }
    }

      class MasterOfPedicure : NailMaster
    {
       
        public override double DoWork()
        {
            Console.WriteLine("Pedicure is done!");
            return 300.0f;
        }

        public override double GetSalary() { return this.Sallary; }
    }

//-------------------------------------------------------------------------
//--------------------------------------------------------------------------
//-----------------------------------------------------------------------
    class Program
    {

  
        static void Main(string[] args)
        {

            int num = 10000000;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
           
            //створення ліста з об"єктами базового класу
            List<IEmployee> figures = new List<IEmployee>();
            for(int i = 0; i< num; i++)
            figures.Add(new Master());
            stopWatch.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);

            //--------------------------------------------------------------------------------
            Stopwatch stopWatch1 = new Stopwatch();
            stopWatch1.Start();
            //створення ліста з об"єктами наслідуваного класу
            List<MasterOfManicure> figures1 = new List<MasterOfManicure>();
            for (int i = 0; i < num; i++)
                figures1.Add(new MasterOfManicure());
            stopWatch1.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts1 = stopWatch1.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime1 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts1.Hours, ts1.Minutes, ts1.Seconds,
                ts1.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime1);

            //---------------------------------------------------------------------------------
            //перелічуваний тип для 
            //Console.WriteLine("Enum:");
            Professions pr1, pr2;
            pr1 = Professions.Colorist;
            pr2 = Professions.HairDresser;

            bool a, b, c, d;
            a = (pr1 == pr2) || ((int)pr2 > 2);
            b = (pr1 > pr2) & ((int)pr2 > 3);
            c = (pr1 == pr2) | ((int)pr2 > 2);
            d = (pr1 > pr2) && ((int)pr2 > 3);


            //Console.WriteLine(a);

           //------------------------------------------------------
           //явне та неявне приведення типів
           Console.WriteLine("Implicit and explicit");
            BeautySalon newSalon1 = new BeautySalon { Name = "Letual'" };
            //явне
            string x = (string)newSalon1;
            Console.WriteLine(x);   
            //неявне
            BeautySalon newSalon2 = x;
            Console.WriteLine(newSalon2.Name);

            //---------------------------------------------------------------------------------
            //перевантажені конструктори base, this
            Console.WriteLine("Base and this");
            Person obj1 = new Person();
            Person obj2 = new Person(firstName: "John");
            Person obj4 = new Person(firstName: "John",  id: 1037);
            Customer obj3 = new Customer(firstName: "John", lastName: "Smith");
            Customer obj5 = new Customer();
            //--------------------------------------------------------------------------------------
            //динаічний конструктор
            dynamic dynamicClass = new DynamicClass();
            string mes = "It is parameter ref";
            string emptyMes;
            //ref
            obj1.SomeMessageRef(ref mes);
            //out
            obj1.SomeMessageOut(out emptyMes);
            Console.WriteLine(emptyMes);

            //-------------------------------------------------------------

            //boxing
            List<object> boxingList = new List<object>();
            for (int i = 0; i < 3; i++)
                boxingList.Add("Boxing list");

            boxingList.Add(3);
            //unboxing
            int unboxingObject = (int)boxingList[3];

            //GC.Collect();
            //---------------------------------------------------------------------------

            Stopwatch stopWatch2 = new Stopwatch();
            stopWatch2.Start();
            for (int i = 0; i < num; i++)
                figures[i] = null;

            stopWatch2.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts2 = stopWatch2.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime2 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts2.Hours, ts2.Minutes, ts2.Seconds,
                ts2.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime2);

            // Get the elapsed time as a TimeSpan value.

            //------------------------------------------------------------------------------
            Stopwatch stopWatch3 = new Stopwatch();
            stopWatch3.Start();
            for (int i = 0; i < num; i++)
                figures[i] = null;

            stopWatch3.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts3 = stopWatch3.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime3 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts3.Hours, ts3.Minutes, ts3.Seconds,
                ts3.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime3);

            // Get the elapsed time as a TimeSpan value.



            Console.ReadLine();

        }

        
    }

   

  
}