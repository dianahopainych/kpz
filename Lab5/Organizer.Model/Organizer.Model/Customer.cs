﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Model
{
    [DataContract]
    public class Customer
    {

        [DataMember]

        public string IdCus { get; set; }
        [DataMember]
        public string NameCus { get; set; }
      
        [DataMember]
        public string PhoneNumberCus { get; set; }
        [DataMember]
        public int OrderCountCus{ get; set; }


    }
}
