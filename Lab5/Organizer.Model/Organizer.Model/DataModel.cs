﻿using System;
using System.Runtime.Serialization;
using System.Text;
using System.Collections.Generic;
using System.IO;



namespace Organizer.Model 
{
    [DataContract]
    public class DataModel
    {
        [DataMember]

        public IEnumerable<Order> Orders { get; set; }


        [DataMember]
        public IEnumerable<Employee> Employees { get; set; }


        [DataMember]
        public IEnumerable<Service> Services { get; set; }

        [DataMember]
        public IEnumerable<Customer> Customers { get; set; }

       




        public DataModel()

        {
            Orders = new List<Order>() { new Order() { Name = "Enter name", Description = "Enter description"} };
            Employees = new List<Employee>() { new Employee() { Id = "Id", Name = "Name"} };
            Services = new List<Service>() { new Service() { Id = "Id", Name = "Service Name" } };
            Customers = new List<Customer>() { new Customer() { IdCus = "Id", NameCus = "First Name"} };
           

        }
        public static string DataPath = "organizer.dat";

        public static DataModel Load()
        {
            if(File.Exists(DataPath))
            {
                return DataSerializier.DeserializeItem(DataPath);
            }
            return new DataModel();

        }

        public void Save()
        {
           
            DataSerializier.SerializeData(DataPath, this);
        }


    }

}