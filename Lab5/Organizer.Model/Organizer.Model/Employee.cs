﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace Organizer.Model
{

    [DataContract]
    public class Employee
    {

        [DataMember]

        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
      
        [DataMember]
        public string Profession { get; set; }

        [DataMember]
        public double Salary { get; set; }
        [DataMember]
        public string Birth { get; set; }
        [DataMember]
        public Sex EmployeeSex { get; set; }

    }

    [DataContract]
    public enum Sex
    {
        [EnumMember]
        Male,
        [EnumMember]
        Female
    }


}