﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace Organizer.Model
{

    [DataContract]
    public class Order
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ServiceIdOrd { get; set; }
        [DataMember]
        public double ServicePriceOrd { get; set; }


        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public OrderStatus  Status { get; set; }

        [DataMember]
        public string EmployeeIdOrd { get; set; }

        [DataMember]
        public string EmployeeNameOrd { get; set; }


        [DataMember]
        public string CustomerIdOrd { get; set; }

        [DataMember]
        public string CustomerNameOrd { get; set; }

      

    }

    [DataContract]
    public enum OrderStatus
    {
        [EnumMember]
        New,
        [EnumMember]
        InProgress,
        [EnumMember]
        Closed
    }


}