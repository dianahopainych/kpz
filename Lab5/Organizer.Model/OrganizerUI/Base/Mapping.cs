﻿using AutoMapper;
using Organizer.Model;
using OrganizerUI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.Profiler;
using OrganizerUI.ViewModels;
//using Task = Organizer.Model.Order;

namespace OrganizerUI.Base
{
    public class Mapping
    {
        public void Create()
        {
            Mapper.CreateMap<DataModel, DataViewModel>();
            Mapper.CreateMap<DataViewModel, DataModel>();

            Mapper.CreateMap<OrderViewModel, Order>();
            Mapper.CreateMap<Order, OrderViewModel>();

            Mapper.CreateMap<EmployeeViewModel, Employee>();
            Mapper.CreateMap<Employee, EmployeeViewModel>();

            Mapper.CreateMap<ServiceViewModel, Service>();
            Mapper.CreateMap<Service, ServiceViewModel>();

            Mapper.CreateMap<CustomerViewModel, Customer>();
            Mapper.CreateMap<Customer, CustomerViewModel>();



        }
    }
}
