﻿using SampleMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Organizer.Model;
using System.Collections.ObjectModel;

namespace OrganizerUI.ViewModels
{
    public class EmployeeViewModel: ViewModelBase
    {


        private string _id;

        public string Id
        {

            get
            {

                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged("Id");

            }

        }

        //---------------------------------------------------------


        private string _name;

        public string Name
        {

           get
           {

              return _name;
           }

           set
           {
            _name = value;
            OnPropertyChanged("Name");

            }

        }
//-----------------------------------------------------
        private double _salary;

        public double Salary
        {

            get
            {

                return _salary;
            }

            set
            {
                _salary = value;
                OnPropertyChanged("Salary");

            }

        }


//-------------------------------------------------
        private string _profession;
        public string Profession
        {

           get
           {
              return _profession;
           }
           set
           {
               _profession = value;
               OnPropertyChanged("Profession");

           }

        }

        //--------------------------------------------


        private string _birth;
        public string Birth
        {

            get
            {
                return _birth;
            }
            set
            {
                _birth = value;
                OnPropertyChanged("Birth");

            }

        }

        //--------------------------------------------
        private Sex _employeeSex;

        public Sex EmployeeSex
    {

           get
           {
                 return _employeeSex;
           }

           set
           {
               _employeeSex = value;
               OnPropertyChanged("EmployeeSex");
           }

        }





      



    }
}
