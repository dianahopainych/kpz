﻿using SampleMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Organizer.Model;

namespace OrganizerUI.ViewModels
{
    public class OrderViewModel : ViewModelBase
    {


        private string _serviceIdOrd;

        public string ServiceIdOrd
        {

            get
            {
                return _serviceIdOrd;

            }

            set
            {

                _serviceIdOrd = value;
                OnPropertyChanged("ServiceIdOrd");
            }

        }


//------------------------------------------------------------
        private string _name;

        public string Name
        {

            get
            {

                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
                
            }

        }

//---------------------------------------------------------------

        private double _servicePriceOrd;

        public double ServicePriceOrd
        {

            get
            {
                return _servicePriceOrd;

            }

            set
            {

                _servicePriceOrd = value;
                OnPropertyChanged("ServicePriceOrd");
            }

        }


//-------------------------------------------------------------------------------

        private string _employeeIdOrd;

        public string EmployeeIdOrd
        {

            get
            {
                return _employeeIdOrd;

            }

            set
            {

                _employeeIdOrd = value;
                OnPropertyChanged("EmployeeIdOrd");
            }

        }

     

//-------------------------------------------------------------

        private string _employeeNameOrd;

        public string EmployeeNameOrd
        {

            get
            {
                return _employeeNameOrd;

            }

            set
            {

                _employeeNameOrd = value;
                OnPropertyChanged("EmployeeNameOrd");
            }

        }



      
//------------------------------------------------------------------



        private string _customerIdOrd;

        public string CustomerIdOrd
        {

            get
            {
                return _customerIdOrd;

            }

            set
            {

                _customerIdOrd = value;
                OnPropertyChanged("CustomerIdOrd");
            }

        }


//------------------------------------------------------------

        private string _customerNameOrd;

        public string CustomerNameOrd
        {

            get
            {
                return _customerNameOrd;

            }

            set
            {

                _customerNameOrd = value;
                OnPropertyChanged("CustomerNameOrd");
            }

        }




//----------------------------------------------------
        

        private string _description;
        public string Description
        {

            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged("Description");

            }

        }

//------------------------------------------------------------------

        private OrderStatus _status;

        public OrderStatus Status
        {

            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }

        }



    }
}
