﻿using SampleMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizerUI.ViewModels
{
    public class ServiceViewModel : ViewModelBase
    {


        private string _id;

        public string Id
        {

            get
            {

                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged("Id");

            }

        }

        //---------------------------------------------------------


        private string _name;

        public string Name
        {

            get
            {

                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged("Name");

            }

        }

        //---------------------------------------------------------

        private double _price;

        public double Price

        {

            get
            {

                return _price;
            }

            set
            {
                _price = value;
                OnPropertyChanged("Price");

            }

        }
              


    
    }
}
