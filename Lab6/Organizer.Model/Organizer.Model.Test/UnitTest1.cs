﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Organizer.Model.Test
{
    [TestClass]
    public class UnitTestSerializer
    {
        [TestMethod]
        public void TestMethodSerializer()
        {

            var model = new DataModel();
           
            model.Orders = new List<Order>() { new Order() {}, new Order() {} };
            model.Employees = new List<Employee>() { new Employee() { Name = "Hello" }, new Employee() { Name = "world!" } };
            model.Services = new List<Service>() { new Service() { Name = "New Service" } };
            model.Customers = new List<Customer>() { new Customer() { Name = "Alice" } };
         

           // DataSerializier.SerializeData(@"D:\serialize_lab5_KPz\organizer.dat", model);

        }


        [TestMethod]
        public void TestMethodDeserialize()
        {
         //   var model = DataSerializier.DeserializeItem(@"D:\serialize_lab5_KPz\organizer.dat");
        }

    }
}
