﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organizer.Model
{
    public class CodeFirstCustomer : DbContext
    {
       

        public CodeFirstCustomer() :
            base("DefaultConnection")
        {
           Database.SetInitializer(new MigrateDatabaseToLatestVersion<CodeFirstCustomer, Organizer.Model.Migrations.Configuration>());
        }

        public DbSet<Customer> Customers { get; set; }
    }
}
