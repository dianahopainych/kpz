﻿namespace Organizer.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrganizerModelv1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomersNew",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        OrderCount = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CustomersNew");
        }
    }
}
