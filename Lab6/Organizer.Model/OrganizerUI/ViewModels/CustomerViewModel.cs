﻿using SampleMVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizerUI.ViewModels
{
    public class CustomerViewModel : ViewModelBase
    {


        private string _idCus;

        public string IdCus
        {

            get
            {

                return _idCus;
            }

            set
            {
                _idCus = value;
                OnPropertyChanged("IdCus");

            }

        }

        //---------------------------------------------------------


        private string _nameCus;

        public string NameCus
        {

            get
            {

                return _nameCus;
            }

            set
            {
                _nameCus = value;
                OnPropertyChanged("NameCus");

            }

        }

        //---------------------------------------------------------

        private string _phoneNumber;
        public string PhoneNumber
        {

            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
                OnPropertyChanged("PhoneNumberCus");

            }

        }

        //--------------------------------------------


        private int _orderCountCus;
        public int OrderCountCus
        {

            get
            {
                return _orderCountCus;
            }
            set
            {
                _orderCountCus= value;
                OnPropertyChanged("OrderCountCus");

            }

        }

  








    }
}
