﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Organizer.Model;
using OrganizerUI.ViewModels;
using SampleMVVM.ViewModels;
using System.Windows.Input;
using System.Windows.Controls;
using System.Data.SqlClient;
using OrganizerUI.Views;

namespace OrganizerUI.ViewModel
{

    public class DataViewModel : ViewModelBase
    {
        public static string SelectedServiceId;
        public static string SelectedCustomerId;
        public static string SelectedEmployeeId;
        public static string SelectedStatus;
        public static string SelectedDescription;
        public DataViewModel()
        {
            SetControlVisibility = new Command(ControlVisibility);
           /// CloseOrderCommand = new Command(CloseOrder);
           // InProgressOrderCommand = new Command(InProgressOrder);
           // FemaleEmployeeCommand = new Command(FemaleEmployee);
           // MaleEmployeeCommand = new Command(MaleEmployee);
           // SetEmployeeIdCommand = new Command(SetEmployeeId);
           // SetEmployeeNameCommand = new Command(SetEmployeeName);
           // SetCustomerIdCommand = new Command(SetCustomerId);
           // SetCustomerNameCommand = new Command(SetCustomerName);
           // SetServiceIdCommand = new Command(SetServiceId);
           // SetServiceNameCommand = new Command(SetServiceName);
           // SetOrderCountCommand = new Command(SetOrderCount);
           // SetSalaryCommand = new Command(SetSalary);

        }



      


     



        private string _visibleControl = "Orders";

        //---------------------------------------------------------
        public string VisibleControl
        {
            get
            {
                return _visibleControl;
            }
            set
            {
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");


            }

        }




        //--------------------------------------------------------
        private OrderViewModel _selectedOrder;

        public OrderViewModel SelectedOrder
        {
            get
            {
                return _selectedOrder;
            }
            set
            {
                _selectedOrder = value;
                OnPropertyChanged("SelectedOrder");



            }

        }


      

        private NewOrderViewModel _selectedNewOrder;

        public NewOrderViewModel SelectedNewOrder
        {
            get
            {
                return _selectedNewOrder;
            }
            set
            {
                _selectedNewOrder = value;
                OnPropertyChanged("SelectedNewOrder");



            }

        }

        private CustomerViewModel _selectedCustomer;

        public CustomerViewModel SelectedCustomer
        {
            get
            {
                return _selectedCustomer;
            }
            set
            {
                _selectedCustomer = value;
                OnPropertyChanged("SelectedCustomer");



            }

        }



        private EmployeeViewModel _selectedEmployee;

        public EmployeeViewModel SelectedEmployee
        {
            get
            {
                return _selectedEmployee;
            }
            set
            {
                _selectedEmployee = value;
                OnPropertyChanged("SelectedEmployee");



            }

        }


        //-------------------------------------------------------------------

        private ObservableCollection<OrderViewModel> _orders;

        public ObservableCollection<OrderViewModel> Orders
        {
            get
            {
                return _orders;
            }
            set
            {
                _orders = value;
                OnPropertyChanged("Orders");


            }

        }

        private ObservableCollection<ChangeOrderViewModel> _changeOrders;

        public ObservableCollection<ChangeOrderViewModel> ChangeOrders
        {
            get
            {
                return _changeOrders;
            }
            set
            {
                _changeOrders = value;
                OnPropertyChanged("ChangeOrders");


            }

        }


        private ObservableCollection<NewOrderViewModel> _newOrders;

        public ObservableCollection<NewOrderViewModel> NewOrders
        {
            get
            {
                return _newOrders;
            }
            set
            {
                _newOrders = value;
                OnPropertyChanged("NewOrders");


            }

        }


        private ObservableCollection<EmployeeViewModel> _employees;
        public ObservableCollection<EmployeeViewModel> Employees
        {


            get
            {
                return _employees;
            }

            set
            {
                _employees = value;
                OnPropertyChanged("Employees");
            }
        }



        private ObservableCollection<ServiceViewModel> _services;
        public ObservableCollection<ServiceViewModel> Services
        {


            get
            {
                return _services;
            }

            set
            {
                _services = value;
                OnPropertyChanged("Services");
            }
        }



        private ObservableCollection<CustomerViewModel> _customers;
        public ObservableCollection<CustomerViewModel> Customers
        {


            get
            {
                return _customers;
            }

            set
            {
                _customers = value;
                OnPropertyChanged("Customers");
            }
        }
        //--------------------------------------------------------------------------------
        public ICommand SetControlVisibility { get; set; }
        public void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }



        //-----------------------------------------------------------------------------------
        //public ICommand CloseOrderCommand { get; set; }

        //public void CloseOrder(object args)
        //{

        //    SelectedOrder.Status = OrderStatus.Closed;
        //}


        //public ICommand InProgressOrderCommand { get; set; }

        //public void InProgressOrder(object args)
        //{

        //    SelectedOrder.Status = OrderStatus.InProgress;
        //}


        //--------------------------------------------------------------------------------




        //public ICommand FemaleEmployeeCommand { get; set; }

        //public void FemaleEmployee(object args)
        //{

        //    SelectedEmployee.EmployeeSex = Sex.Female;
        //}



        //public ICommand MaleEmployeeCommand { get; set; }

        //public void MaleEmployee(object args)
        //{

        //    SelectedEmployee.EmployeeSex = Sex.Male;
        //}



        //----------------------------------------------------------




        //public ICommand SetEmployeeIdCommand { get; set; }

        //public void SetEmployeeId(object args)
        //{

        //    //SelectedEmployee.EmployeeSex = Sex.Female;
        //    foreach (EmployeeViewModel employee in _employees)
        //    {
        //        if (employee.Name == SelectedOrder.EmployeeNameOrd)
        //            SelectedOrder.EmployeeIdOrd = employee.Id;
        //    }

        //}



        //public ICommand SetEmployeeNameCommand { get; set; }

        //public void SetEmployeeName(object args)
        //{

        //    //SelectedEmployee.EmployeeSex = Sex.Female;
        //    foreach (EmployeeViewModel employee in _employees)
        //    {
        //        if (employee.Id == SelectedOrder.EmployeeIdOrd)
        //        {
        //            SelectedOrder.EmployeeNameOrd = employee.Name;

        //        }

        //    }

        //}


        //----------------------------------------------------------



        //public ICommand SetCustomerIdCommand { get; set; }

        //public void SetCustomerId(object args)
        //{

        //    //SelectedEmployee.EmployeeSex = Sex.Female;
        //    foreach (CustomerViewModel customer in _customers)
        //    {
        //        if (customer.NameCus == SelectedOrder.CustomerNameOrd)
        //            SelectedOrder.CustomerIdOrd = customer.IdCus;
        //    }

        //}




        //public ICommand SetCustomerNameCommand { get; set; }

        //public void SetCustomerName(object args)
        //{

        //    //SelectedEmployee.EmployeeSex = Sex.Female;
        //    foreach (CustomerViewModel customer in _customers)
        //    {
        //        if (customer.IdCus == SelectedOrder.CustomerIdOrd)
        //        {
        //            SelectedOrder.CustomerNameOrd = customer.NameCus;

        //        }

        //    }

        //}


        //----------------------------------------------------------


        //public ICommand SetServiceIdCommand { get; set; }

        //public void SetServiceId(object args)
        //{

        //    foreach (ServiceViewModel service in _services)
        //    {
        //        if (service.Name == SelectedOrder.Name)
        //        {
        //            SelectedOrder.ServiceIdOrd = service.Id;
        //            SelectedOrder.ServicePriceOrd = service.Price;
        //        }
        //    }

        //}





        //public ICommand SetServiceNameCommand { get; set; }

        //public void SetServiceName(object args)
        //{
        //    foreach (ServiceViewModel service in _services)
        //    {
        //        if (service.Id == SelectedOrder.ServiceIdOrd)
        //        {
        //            SelectedOrder.Name = service.Name;
        //            SelectedOrder.ServicePriceOrd = service.Price;

        //        }

        //    }

        //}


        //----------------------------------------------------------
        //public ICommand SetOrderCountCommand { get; set; }

        //public void SetOrderCount(object args)
        //{
        //    SelectedCustomer.OrderCountCus = 0;

        //    foreach (OrderViewModel order in _orders)
        //    {
        //        if (order.CustomerIdOrd == SelectedCustomer.IdCus)
        //        {


        //            SelectedCustomer.OrderCountCus += 1;

        //        }

        //    }

        //}





        //public ICommand SetSalaryCommand { get; set; }

        //public void SetSalary(object args)
        //{

        //    SelectedEmployee.Salary = 0;
        //    foreach (OrderViewModel order in _orders)
        //    {


        //        if (order.EmployeeIdOrd == SelectedEmployee.Id)
        //        {

        //            SelectedEmployee.Salary += order.ServicePriceOrd;

        //        }



        //    }


        //}





    }
}
