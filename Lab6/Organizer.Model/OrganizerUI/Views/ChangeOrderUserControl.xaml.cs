﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OrganizerUI.ViewModel;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for ChangeOrderUserControl.xaml
    /// </summary>
    public partial class ChangeOrderUserControl : UserControl
    {
        string connectionString;
        SqlDataAdapter adapter;
        DataTable customersTable;

        public ChangeOrderUserControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {



           string connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";

            string sql = string.Format("UPDATE Orders SET ServiceId = '{0}', CustomerId = '{1}'," +
               "EmployeeId = '{2}', Status = '{3}', Description = '{4}' " +
               "WHERE Id = '{5}'",
                  TextBoxService.Text, TextBoxCustomer.Text, TextBoxEmployee.Text,
               TextBoxStatus.Text, TextBoxDescription.Text, TextBoxId.Text); 
            customersTable = new DataTable();
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, connection);
                adapter = new SqlDataAdapter(command);


                connection.Open();
                adapter.Fill(customersTable);
               // DataGridCustomers.ItemsSource = customersTable.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            MessageBox.Show("Item has changed successfully!");






        }
    }
}
