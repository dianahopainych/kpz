﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using Organizer.Model;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for CustomersUserControl.xaml
    /// </summary>
    public partial class CustomersUserControl : UserControl
    {
        public static DataGrid datagrid;
        ForKPZEntities1 db = new ForKPZEntities1();

        //string connectionString;
        //SqlDataAdapter adapter;
        //DataTable customersTable;
        public CustomersUserControl()
        {
            InitializeComponent();
            Load(); ;
           // string connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";
            //db = new CodeFirstCustomer();
            //db.Customers.Load(); // загружаем данные
            //DataGridCustomers.ItemsSource = db.Customers.Local.ToBindingList(); // устанавливаем привязку к кэшу

           // this.Closing += MainWindow_Closing;
        }



        private void Load()
        {
            DataGridCustomers.ItemsSource = db.Customers.ToList();
            datagrid = DataGridCustomers;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            //connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";

            //string sql = "SELECT * FROM Customers";
            //customersTable = new DataTable();
            //SqlConnection connection = null;
            //try
            //{
            //    connection = new SqlConnection(connectionString);
            //    SqlCommand command = new SqlCommand(sql, connection);
            //    adapter = new SqlDataAdapter(command);


            //    connection.Open();
            //    adapter.Fill(customersTable);
            //    DataGridCustomers.ItemsSource = customersTable.DefaultView;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //finally
            //{
            //    if (connection != null)
            //        connection.Close();
            //}
            //ShowCustomers();


           
            


        }





        private void Button_Click1(object sender, RoutedEventArgs e)
        {

            //if (DataGridCustomers.SelectedItems != null)
            //{
            //    for (int i = 0; i < DataGridCustomers.SelectedItems.Count; i++)
            //    {
            //        DataRowView datarowView = DataGridCustomers.SelectedItems[i] as DataRowView;
            //        if (datarowView != null)
            //        {
            //            DataRow dataRow = (DataRow)datarowView.Row;
            //            dataRow.Delete();
            //        }
            //    }
            //}
            //ShowCustomers();
        }

        private void insertBtn_Click(object sender, RoutedEventArgs e)
        {
            InsertWindow Ipage = new InsertWindow();
            Ipage.ShowDialog();
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            int Id = (DataGridCustomers.SelectedItem as Customer).Id;
            var deleteMember = db.Customers.Where(m => m.Id == Id).Single();
            db.Customers.Remove(deleteMember);
            db.SaveChanges();
            DataGridCustomers.ItemsSource = db.Customers.ToList();
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            int Id = (DataGridCustomers.SelectedItem as Customer).Id;
            UpdateWindow Upage = new UpdateWindow(Id);
            Upage.ShowDialog();
        }

        //public void ShowCustomers()
        //{
        //    SqlCommandBuilder comandbuilder = new SqlCommandBuilder(adapter);
        //    adapter.Update(customersTable);
        //}





    }
}
