﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for EmployeeControl.xaml
    /// </summary>
    public partial class EmployeeControl : UserControl
    {

        string connectionString;
        SqlDataAdapter adapter;
        DataTable employeesTable;


        public EmployeeControl()
        {
            InitializeComponent();
            connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void DataGridEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";

            string sql = "SELECT * FROM Employees";
            employeesTable = new DataTable();
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, connection);
                adapter = new SqlDataAdapter(command);


                connection.Open();
                adapter.Fill(employeesTable);
                DataGridEmployees.ItemsSource = employeesTable.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            ShowEmployees();
        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (DataGridEmployees.SelectedItems != null)
            {
                for (int i = 0; i < DataGridEmployees.SelectedItems.Count; i++)
                {
                    DataRowView datarowView = DataGridEmployees .SelectedItems[i] as DataRowView;
                    if (datarowView != null)
                    {
                        DataRow dataRow = (DataRow)datarowView.Row;
                        dataRow.Delete();
                    }
                }
            }
            ShowEmployees();

        }



        public void ShowEmployees()
        {
            SqlCommandBuilder comandbuilder = new SqlCommandBuilder(adapter);
            adapter.Update(employeesTable);
        }








    }
}
