﻿using Organizer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for InsertWindow.xaml
    /// </summary>
    public partial class InsertWindow : Window
    {
        ForKPZEntities1 db = new ForKPZEntities1();
        public InsertWindow()
        {
            InitializeComponent();
        }

        private void insertBtn_Click(object sender, RoutedEventArgs e)
        {
            Customer newMember = new Customer()
            {
                Name = nametextBox1.Text,
                PhoneNumber = nametextBox2.Text,
                OrderCount = Int32.Parse(nametextBox3.Text)
            };

            db.Customers.Add(newMember);
            db.SaveChanges();
            CustomersUserControl.datagrid.ItemsSource = db.Customers.ToList();
            this.Hide();
        }
    }
}
