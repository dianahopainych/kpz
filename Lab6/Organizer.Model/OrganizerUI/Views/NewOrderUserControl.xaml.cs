﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrganizerUI.Views
{
   
    /// <summary>
    /// Interaction logic for NewOrderUserControl.xaml
    /// </summary>
    public partial class NewOrderUserControl : UserControl
    {
        public NewOrderUserControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection ss = new SqlConnection("Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True");
            ss.Open();
            SqlDataAdapter aa = new SqlDataAdapter("ProcedureNewOrder", ss);
            aa.SelectCommand.CommandType = CommandType.StoredProcedure;

            aa.SelectCommand.Parameters.Add("@ServiceName", SqlDbType.VarChar, (50)).Value = TextBoxService.Text;
            aa.SelectCommand.Parameters.Add("@CusName", SqlDbType.VarChar, (50)).Value = TextBoxCustomer.Text;
            aa.SelectCommand.Parameters.Add("@EmpName", SqlDbType.VarChar, (50)).Value = TextBoxEmployee.Text;
            aa.SelectCommand.Parameters.Add("@Status", SqlDbType.VarChar, (50)).Value = TextBoxStatus.Text;
            aa.SelectCommand.Parameters.Add("@Descripton", SqlDbType.VarChar, (200)).Value = TextBoxDescription.Text;


              aa.SelectCommand.ExecuteNonQuery();
            ss.Close();
            MessageBox.Show("Item has added successfully!");
        }
    }
}
