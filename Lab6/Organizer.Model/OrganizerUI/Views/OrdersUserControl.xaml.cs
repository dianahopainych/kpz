﻿using OrganizerUI.Convertors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OrganizerUI.ViewModel;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for TasksUserControl.xaml
    /// </summary>
    public partial class TasksUserControl : UserControl
    {
        
        string connectionString;
        SqlDataAdapter adapter;
        DataTable ordersTable;
        DataViewModel m = new DataViewModel();

        public TasksUserControl()
        {
            InitializeComponent();
            connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";


        }

        private void DataGridTasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void DataGridTasks_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";

            string sql = "SELECT * FROM Orders";
            ordersTable = new DataTable();
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, connection);
                adapter = new SqlDataAdapter(command);


                connection.Open();
                adapter.Fill(ordersTable);
                DataGridOrders.ItemsSource = ordersTable.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            ShowOrders();
        }





        public void ShowOrders()
        {
            SqlCommandBuilder comandbuilder = new SqlCommandBuilder(adapter);
            adapter.Update(ordersTable);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (DataGridOrders.SelectedItems != null)
            {
                for (int i = 0; i < DataGridOrders.SelectedItems.Count; i++)
                {
                    DataRowView datarowView = DataGridOrders.SelectedItems[i] as DataRowView;
                    if (datarowView != null)
                    {
                        DataRow dataRow = (DataRow)datarowView.Row;
                        dataRow.Delete();
                    }
                }
            }
            ShowOrders();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
          
 
            
        }
    }
}
