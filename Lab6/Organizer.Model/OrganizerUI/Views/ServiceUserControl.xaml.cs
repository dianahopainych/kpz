﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for ServiceUserControl.xaml
    /// </summary>
    public partial class ServiceUserControl : UserControl
    {

        string connectionString;
        SqlDataAdapter adapter;
        DataTable servicesTable;
        public ServiceUserControl()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            connectionString = "Data Source=DESKTOP-L65EMDQ\\SQLEXPRESS;Initial Catalog=ForKPZ;Integrated Security=True";

            string sql = "SELECT * FROM Services";
            servicesTable = new DataTable();
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, connection);
                adapter = new SqlDataAdapter(command);


                connection.Open();
                adapter.Fill(servicesTable);
                DataGridServices.ItemsSource = servicesTable.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            ShowServices();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           

        }



        public void ShowServices()
        {
            SqlCommandBuilder comandbuilder = new SqlCommandBuilder(adapter);
            adapter.Update(servicesTable);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (DataGridServices.SelectedItems != null)
            {
                for (int i = 0; i < DataGridServices.SelectedItems.Count; i++)
                {
                    DataRowView datarowView = DataGridServices.SelectedItems[i] as DataRowView;
                    if (datarowView != null)
                    {
                        DataRow dataRow = (DataRow)datarowView.Row;
                        dataRow.Delete();
                    }
                }
            }
            ShowServices();
        }
    }
}
