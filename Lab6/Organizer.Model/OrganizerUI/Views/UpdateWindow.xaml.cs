﻿using Organizer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrganizerUI.Views
{
    /// <summary>
    /// Interaction logic for UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        int Id;
        ForKPZEntities1 db = new ForKPZEntities1();
        public UpdateWindow(int memberId)
        {
            InitializeComponent();
            Id = memberId;
        }

        private void updateBtn_Click_3(object sender, RoutedEventArgs e)
        {
            Customer updateMember = (from m in db.Customers 
                                   where m.Id == Id
                                   select m).Single();
            updateMember.Name = nametextBox1.Text;
            updateMember.PhoneNumber = nametextBox2.Text;
            updateMember.OrderCount  = Int32.Parse(nametextBox3.Text);
            db.SaveChanges();
            CustomersUserControl.datagrid.ItemsSource = db.Customers.ToList();
            this.Hide();
        }
    }
}
