﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;

namespace WebApplication2.Models
{
    

    public class CustomerContext : System.Data.Entity.DbContext
    {
        static string name = "CustomerContext";
        public CustomerContext()
            : base("name=CustomerContext")
        {
        }
        public System.Data.Entity.DbSet<Customer> Customers { get; set; }
    }

    public class CustomerRepository : IDisposable
    {
        private CustomerContext db = new CustomerContext();

        public IEnumerable<Customer> GetAll()
        {
            return db.Customers;
        }
        public Customer GetByID(string id)
        {
            return db.Customers.FirstOrDefault(p => p.Id == id);
        }
        public void Add(Customer customer)
        {
            db.Customers.Add(customer);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}